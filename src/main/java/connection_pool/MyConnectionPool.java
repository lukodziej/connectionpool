package connection_pool;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;


public class MyConnectionPool {
    private static final int INITIAL_POOL_SIZE = 10;
    private static final int MAX_POOL_SIZE = 100;
    private final List<SingleConnection> connectionPool = new ArrayList<>(INITIAL_POOL_SIZE);
    private final Semaphore poolSemaphore = new Semaphore(1);
    private int counter = 0;

    private SingleConnection singleConnection;

    public void createInitialPool() throws SQLException {
        for (int i = 0; i < INITIAL_POOL_SIZE; i++) {
            System.out.println("CREATING POOL");
            addConnectionToPool();
        }
    }

    private SingleConnection addConnectionToPool() throws SQLException {
        singleConnection = new SingleConnection();
        connectionPool.add(singleConnection);
        return singleConnection;
    }

    private List<SingleConnection> getFreeConnectionList() {
        return connectionPool.stream().filter(SingleConnection::isFree).toList();
    }

    private long getFreeConnectionsAmount() {
        return connectionPool.stream().filter(SingleConnection::isFree).count();
    }

    private void takeConnection(SingleConnection singleConnection) {
        singleConnection.setFree(false);
    }

    public void returnConnection(SingleConnection singleConnection) {
        singleConnection.setFree(true);
    }

    public SingleConnection getFreeConnection() throws SQLException, InterruptedException {
        poolSemaphore.acquire();
        try {
            if (++counter % 200 == 0) removeExcessiveConnections();
            if (connectionPool.stream().anyMatch(SingleConnection::isFree)) {
                singleConnection = connectionPool.stream().filter(SingleConnection::isFree).findFirst().get();
                takeConnection(singleConnection);
                System.out.println("FREE CONNECTIONS LEFT: " + getFreeConnectionsAmount());
            } else {
                if (connectionPool.size() < MAX_POOL_SIZE) {
                    System.out.println("ADDING NEW CONNECTIONS");
                    singleConnection = addConnectionToPool();
                    takeConnection(singleConnection);
                } else {
                    System.out.println("NO FREE CONNECTIONS AVAILABLE, TRY AGAIN LATER!");
                }
            }
        } finally {
            poolSemaphore.release();
        }
        return singleConnection;
    }

    private void removeExcessiveConnections() throws SQLException {
        System.out.println("-------------------------------------------------REMOVING PROCEDURE-----------------------------------------------------");
        System.out.println("FREE CONNECTIONS IN POOL: " + getFreeConnectionsAmount());
        while (getFreeConnectionsAmount() > INITIAL_POOL_SIZE) {
            System.out.println("DELETING FREE SINGLE CONNECTION, STILL IN POOL: " + getFreeConnectionsAmount());
            singleConnection = getFreeConnectionList().get(0);
            singleConnection.closeConnection();
            connectionPool.remove(singleConnection);
        }
    }

    // Only for cleaning after test
    public void removeAllConnections() {
        System.out.println("Removing all connections!");
        while (!connectionPool.isEmpty()) {
            connectionPool.remove(0);
        }
    }
}