import connection_pool.MyConnectionPool;
import connection_pool.SingleConnection;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class MyConnectionPoolTest {

    MyConnectionPool myConnectionPool = new MyConnectionPool();

    @BeforeEach
    void createPool() throws SQLException {
        myConnectionPool.createInitialPool();
    }

    @AfterEach
    void removeAll() throws SQLException, InterruptedException {
        myConnectionPool.getFreeConnection().createConnection().createStatement().executeUpdate("delete from data where Username ='test'");
        myConnectionPool.removeAllConnections();
    }


    @Test
    void heavyLoadTest() throws InterruptedException, SQLException {
        ExecutorService executor = Executors.newFixedThreadPool(10000);
        for (int i = 0; i < 10000; i++) {
            executor.submit(() -> {
                try {
                    SingleConnection connection = myConnectionPool.getFreeConnection();
                    connection.getConnection().createStatement().executeUpdate("INSERT INTO data VALUES ('test', 'test', 0, 'test')");
                    myConnectionPool.returnConnection(connection);
                } catch (SQLException | InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        ResultSet rs = myConnectionPool.getFreeConnection().createConnection().createStatement().executeQuery("select count(*) from data where Username ='test'");
        rs.next();
        int counter = rs.getInt(1);
        Assertions.assertEquals(10000, counter);
    }

}

