import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static database.tables.Data.DATA;

public class DatabaseTimeTests {

    private static final String USER = "root";
    private static final String PASSWORD = "admin";
    private static final String URL = "jdbc:mysql://localhost:3306/users";
    private DSLContext create = null;


    public static void main(String[] args) throws SQLException {
        DatabaseTimeTests databaseTests = new DatabaseTimeTests();
        databaseTests.connectEveryTime();
        databaseTests.connectOnce();
    }

    public void connectEveryTime() throws SQLException {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
            create = DSL.using(conn, SQLDialect.MYSQL);
            create.select(DATA.USERNAME).from(DATA).fetch();
        }
        long timeNeeded = ((System.currentTimeMillis()) - startTime);
        System.out.println("Connect every time executing time: " + (double) timeNeeded / 1000 + " sec.");
    }

    public void connectOnce() throws SQLException {
        long startTime = System.currentTimeMillis();
        Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
        create = DSL.using(conn, SQLDialect.MYSQL);
        for (int i = 0; i < 1000; i++) {
            create.select(DATA.USERNAME).from(DATA).fetch();
        }
        long timeNeeded = ((System.currentTimeMillis()) - startTime);
        System.out.println("Connect once executing time: " + (double) timeNeeded / 1000 + " sec.");
    }


}
